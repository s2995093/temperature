package nl.utwente.di.temperatureQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/* Tests the Quoter */
public class TestQuoter {
    @Test
    public void testBook1() throws Exception {
        Quoter quoter = new Quoter();
        double price = quoter.returnFahrenheit("60");
        Assertions.assertEquals(60, price);
    }
}

