package nl.utwente.di.temperatureQuote;

public class Quoter {

    public double returnFahrenheit(String celsius) {
        int value = Integer.parseInt(celsius);
        return (value * 9/5) + 32;
    }
}
